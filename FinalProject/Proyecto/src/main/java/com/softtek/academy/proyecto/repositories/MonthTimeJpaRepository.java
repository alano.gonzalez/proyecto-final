package com.softtek.academy.proyecto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.User;


public interface MonthTimeJpaRepository extends JpaRepository<MonthTime, Integer>{
	public List<MonthTime> findAllByUser(User u);
}
