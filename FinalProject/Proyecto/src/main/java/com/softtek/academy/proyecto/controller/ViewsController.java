package com.softtek.academy.proyecto.controller;

import java.time.DateTimeException;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.proyecto.model.GetHolder;
import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.PostHolder;
import com.softtek.academy.proyecto.model.PostPeriodUserResponse;
import com.softtek.academy.proyecto.model.SpentTime;
import com.softtek.academy.proyecto.model.User;
import com.softtek.academy.proyecto.service.UserService;

@Controller
public class ViewsController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/userMonth", method = RequestMethod.GET)
	public ModelAndView userForMonth() {
		return new ModelAndView("userMonth", "command", new GetHolder());
	}
	
	@RequestMapping(value="/userMonthview", method = RequestMethod.POST)
	 public String viewPersons(@ModelAttribute("SpringWeb")GetHolder holder, ModelMap model) {
		List<SpentTime> resultlist = new ArrayList<>();
		SpentTime st = new SpentTime();
		try {
			Double time = new Double(0);
			Month m = Month.of(holder.getMes());
			User user = userService.getUserDB(holder.getIS());
			if(user!=null) {
				for(MonthTime mt : user.getMonths()) {
					if(mt.getMonth().equals(holder.getMes())) {
						time = mt.getTime();
						break;
					}
				}
				st.setIS(holder.getIS());
			}
			else {
				st.setIS("No existe el Usuario");
			}
			st.setMonth(m.toString());
			st.setTime(time);
		}catch(DateTimeException e) {
			st.setIS(holder.getIS());
			st.setMonth("No existe el mes");
			st.setTime(new Double(0));
		}
		resultlist.add(st);
		model.addAttribute("list", resultlist);
		return "monthuserresult";
	}
	
	@RequestMapping(value="/usersMonth", method = RequestMethod.GET)
	public ModelAndView usersForMonth() {
		return new ModelAndView("usersMonth", "command", new GetHolder());
	}
	
	@RequestMapping(value="/usersMonthview", method = RequestMethod.POST)
	 public String viewMPersons(@ModelAttribute("SpringWeb")GetHolder holder, ModelMap model) {
		model.addAttribute("list", userService.getMonthTimeUsersDB(holder.getMes()));
		return "monthuserresult";
	}
	
	@RequestMapping(value="/userPeriod", method = RequestMethod.GET)
	public String userPeriod() {
		return "perioduser";
	}

	@RequestMapping(value="/userPeriodView", method = RequestMethod.POST)
	 public String viewPeriodUser(@RequestBody String postparams, ModelMap model) {
		PostHolder ph = userService.createPostHolder(postparams);
		List<PostPeriodUserResponse> list = new ArrayList<>();
		list.add(userService.getPeriodTimeForUser(ph));
		model.addAttribute("list", list);
		return "periodresult";
	}
	
	@RequestMapping(value="/usersPeriod", method = RequestMethod.GET)
	public String usersPeriod() {
		return "periodusers";
	}

	@RequestMapping(value="/usersPeriodView", method = RequestMethod.POST)
	 public String viewPeriodUsers(@RequestBody String postparams, ModelMap model) {
		PostHolder ph = userService.createPostHolder(postparams);
		model.addAttribute("list", userService.getPeriodTimeUsers(ph));
		return "periodresult";
	}
	
	
}
