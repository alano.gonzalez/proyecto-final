package com.softtek.academy.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Id;

@Entity
public class MonthTime {

	@Id
	@Column(name="MTId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Id;
	@Column(name = "monthNum")
	private Integer month;
	@Column(name = "timespent")
	private Double time;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private User user;
	
	
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Double getTime() {
		return time;
	}
	public void setTime(Double time) {
		this.time = time;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "MonthTime [Month=" + month + ", Hours=" + time + "]";
	}
	

}
