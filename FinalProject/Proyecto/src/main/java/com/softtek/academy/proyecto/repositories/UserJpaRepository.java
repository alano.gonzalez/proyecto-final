package com.softtek.academy.proyecto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.softtek.academy.proyecto.model.User;

public interface UserJpaRepository extends JpaRepository<User, Integer>{
	User findFirstByIS(String IS);
	
	@Query("SELECT u from User u INNER JOIN u.months m where m.month = ?1")
	List<User> fetchUserMonthTimeInnerJoin(Integer month);
	
	
}
