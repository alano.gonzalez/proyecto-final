package com.softtek.academy.proyecto.model;

public class GetHolder {

	private String IS;
	private Integer mes;
	
	public GetHolder() {}
	
	public String getIS() {
		return IS;
	}
	public void setIS(String iS) {
		IS = iS;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	
	
	
}
