package com.softtek.academy.proyecto.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostPeriodUserResponse {

	private String IS;
	private Double time;
	private List<String> registers = new ArrayList<>();
	
	public PostPeriodUserResponse() {}
	
	public PostPeriodUserResponse(String IS, Double time) {
		this.IS = IS;
		this.time = time;
	}

	public String getIS() {
		return IS;
	}

	public void setIS(String iS) {
		IS = iS;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

	public List<String> getRegisters() {
		return registers;
	}

	public void setRegisters(List<String> registers) {
		this.registers = registers;
	}
	
	public void addRegister(String reg) {
		this.registers.add(reg);
	}
	
	
}
