package com.softtek.academy.proyecto.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.proyecto.daos.UserExcelDao;
import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.PostHolder;
import com.softtek.academy.proyecto.model.PostPeriodUserResponse;
import com.softtek.academy.proyecto.model.SpentTime;
import com.softtek.academy.proyecto.model.User;
import com.softtek.academy.proyecto.repositories.MonthTimeJpaRepository;
import com.softtek.academy.proyecto.repositories.UserJpaRepository;

@Service("userService")
public class UserServiceImpl implements UserService{
	@Autowired
	UserExcelDao userExcelDao;
	
	@Autowired
	UserJpaRepository userJpaRepository;
	
	@Autowired
	MonthTimeJpaRepository monthTimeJpaRepository;
	

	@Override
	public User getUserExcel(String IS) {
		return userExcelDao.getUserExcel(IS);
	}

	@Override
	public Double getHrsByMonth(User u, int mes) {
		Double hrs = new Double(0);
		List <Date> fechas = u.getRegisters();
		for(int i=0;i<fechas.size();i++) {
			if(fechas.get(i).getMonth()+1==mes) {
				if(i+1<fechas.size()) {
					if(fechas.get(i).getDate() == fechas.get(i+1).getDate()) {
						Double d =  (double) (fechas.get(i+1).getTime() - fechas.get(i).getTime());
						d/=1000;
						d/=60;
						d/=60;
						hrs += d;
					}
				}
			}
		}
		MonthTime nuevoMes = new MonthTime();
		nuevoMes.setMonth(mes);
		nuevoMes.setTime(hrs);
		nuevoMes.setUser(u);
		if(hrs>0){
			u.getMonths().add(nuevoMes);	
		}
		return hrs;
	}

	@Override
	public User getUserDB(String IS) {
		User user = userJpaRepository.findFirstByIS(IS);
		if(user!=null) {
			user.setMonths(monthTimeJpaRepository.findAllByUser(user));
		}
		return user;
	}

	@Override
	public void persistUserDB(User user) {
		userJpaRepository.save(user);
	}

	@Override
	public void persistMonth(MonthTime monthTime) {
		monthTimeJpaRepository.save(monthTime);
	}

	@Override
	public PostHolder createPostHolder(String postparams) {
		PostHolder ph = new PostHolder();
		String[] params = postparams.split("&");
		String IS = "";
		String startDate = "";
		String endDate = "";
		if(params.length==3) {
			 IS = params[0].split("=")[1];
			 startDate = params[1].split("=")[1];
			 endDate = params[2].split("=")[1];
		}else if(params.length==2){
			startDate = params[0].split("=")[1];
			 endDate = params[1].split("=")[1];
		}
		try {
			Date start = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
			Date end = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
			ph.setEndDate(end);
			ph.setStartDate(start);
			ph.setIS(IS);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ph;
	}

	@Override
	public PostPeriodUserResponse getPeriodTimeForUser(PostHolder ph) {
		User user = new User();
		user = getUserExcel(ph.getIS());
		Double p = new Double(0);
		PostPeriodUserResponse period = new PostPeriodUserResponse();
		List<Date> periodDates = new ArrayList<>();
		if(user.getRegisters().size()>0) {
			for(Date date : user.getRegisters()) {
				if(date.getTime()>=ph.getStartDate().getTime() && date.getTime()<=ph.getEndDate().getTime() + 86400000) {
					periodDates.add(date);
				}
			}
		}
		for(int i=0;i<periodDates.size();i++) {
			period.addRegister(periodDates.get(i).toString());
			if(i+1<periodDates.size()) {
				if(periodDates.get(i).getDate() == periodDates.get(i+1).getDate()) {
					Double d =  (double) (periodDates.get(i+1).getTime() - periodDates.get(i).getTime());
					d/=1000;
					d/=60;
					d/=60;
					p+=d;
				}
			}
		}
		period.setIS(ph.getIS());
		period.setTime(p);
		return period;
	}

	@Override
	public List<SpentTime> getMonthTimeUsersDB(Integer mes) {
		List<SpentTime> times = new ArrayList<>();
		List<User>users  = userJpaRepository.fetchUserMonthTimeInnerJoin(mes);
		try {
		Month m = Month.of(mes);
		for(User user:users) {
			for(MonthTime mt : user.getMonths()) {
				if(mt.getMonth().equals(mes)) {
					times.add(new SpentTime(m.toString(), mt.getTime(), user.getIS()));
				}
			}
		}
		if(times.size()==0) {
			times.add(new SpentTime("Ningun usuario trabajó en el mes " + m.toString(), new Double(0), "Nadie"));
		}
		}catch(DateTimeException e) {
			times.add(new SpentTime("No existe el mes", new Double(0), "Nadie"));
		}
		return times;
	}

	@Override
	public void persistAll() {
		userJpaRepository.deleteAll();
		monthTimeJpaRepository.deleteAll();
		List<User> users = userExcelDao.getUsers();
		for(User user : users) {
			for(int i=1;i<13;i++) {
				getHrsByMonth(user, new Integer(i));
			}
		}
		userJpaRepository.save(users);
	}

	@Override
	public List<PostPeriodUserResponse> getPeriodTimeUsers(PostHolder ph) {
		List<PostPeriodUserResponse> times = new ArrayList<>();
		List<User> users = userExcelDao.getUsersNotReload();
		for(User user : users) {
			Double time = new Double(0);
			List<Date> periodDates = user.getRegisters();
			PostPeriodUserResponse periodresp = new PostPeriodUserResponse();
			for(int i=0;i<periodDates.size();i++) {
				if(periodDates.get(i).getTime()>=ph.getStartDate().getTime() && periodDates.get(i).getTime()<=ph.getEndDate().getTime() + 86400000) {
					periodresp.addRegister(periodDates.get(i).toString());
					if(i+1<periodDates.size()) {
						if(periodDates.get(i).getDate() == periodDates.get(i+1).getDate()) {
							Double aux =  (double) (periodDates.get(i+1).getTime() - periodDates.get(i).getTime());
							aux/=1000;
							aux/=60;
							aux/=60;
							time+=aux;
						}
					}
				}
			}
			periodresp.setTime(time);
			periodresp.setIS(user.getIS());
			if(time>0){
				times.add(periodresp);	
			}
		}
		if(times.size()==0) {
			times.add(new PostPeriodUserResponse("Ningun usuario trabajó en el periodo " +ph.getStartDate() + " | "  +
		ph.getEndDate(), new Double(0)));
		}
		return times;
	}

}
