package com.softtek.academy.proyecto.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class User {
	
	@Column(name = "uIS")
	private String iS;
	@Id
	private Integer Id;
	@Transient
	private List<Date> registers = new ArrayList<>();
	
	@OneToMany(
			mappedBy = "user",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.EAGER)
	private List<MonthTime> months = new ArrayList<>();
	
	public User() {}
	
	public List<MonthTime> getMonths() {
		return months;
	}

	public void setMonths(List<MonthTime> months) {
		this.months = months;
	}

	
	
	public List<Date> getRegisters() {
		return registers;
	}
	
	public void addRegister(Date reg) {
		this.registers.add(reg);
	}

	public void setRegisters(List<Date> registers) {
		this.registers = registers;
	}

	public String getIS() {
		return iS;
	}
	public void setIS(String IS) {
		this.iS = IS;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer Id) {
		this.Id = Id;
	}

	@Override
	public String toString() {
		return "User: " + iS + ", " + Id + ",  " + registers.size(); 
	}
		
	
}