package com.softtek.academy.proyecto.model;

public class SpentTime {

	private String month;
	private Double time;
	private String IS;
	
	public SpentTime() {}
	
	public SpentTime(String month, Double time, String IS) {
		super();
		this.month = month;
		this.time = time;
		this.IS = IS;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Double getTime() {
		return time;
	}
	public void setTime(Double time) {
		this.time = time;
	}

	public String getIS() {
		return IS;
	}

	public void setIS(String iS) {
		IS = iS;
	}
	
	
	
}
