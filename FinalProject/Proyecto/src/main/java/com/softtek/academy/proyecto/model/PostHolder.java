package com.softtek.academy.proyecto.model;

import java.util.Date;

public class PostHolder {

	private String IS;
	private Date startDate;
	private Date endDate;
	
	public PostHolder() {}

	public String getIS() {
		return IS;
	}

	public void setIS(String iS) {
		IS = iS;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}
