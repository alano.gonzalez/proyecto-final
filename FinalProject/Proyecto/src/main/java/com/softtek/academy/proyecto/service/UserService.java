package com.softtek.academy.proyecto.service;

import java.util.List;

import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.PostHolder;
import com.softtek.academy.proyecto.model.PostPeriodUserResponse;
import com.softtek.academy.proyecto.model.SpentTime;
import com.softtek.academy.proyecto.model.User;


public interface UserService {
	
	public User getUserExcel(String IS);
	public Double getHrsByMonth(User u, int mes);
	public User getUserDB(String IS);
	public void persistAll();
	public void persistUserDB(User user);
	public void persistMonth(MonthTime monthTime);
	public PostHolder createPostHolder(String postparams);
	public PostPeriodUserResponse getPeriodTimeForUser(PostHolder ph);
	public List<SpentTime> getMonthTimeUsersDB(Integer mes);
	public List<PostPeriodUserResponse> getPeriodTimeUsers(PostHolder ph);
	
}
