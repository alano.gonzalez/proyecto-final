package com.softtek.academy.proyecto.daos;

import java.util.List;

import org.springframework.stereotype.Component;

import com.softtek.academy.proyecto.model.User;


public interface UserExcelDao {
	public User getUserExcel(String IS);
	public List<User> getUsers();
	List<User> getUsersNotReload();
	
}
