package com.softtek.academy.proyecto.controller;

import java.time.DateTimeException;
import java.time.Month;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.PostHolder;
import com.softtek.academy.proyecto.model.PostPeriodUserResponse;
import com.softtek.academy.proyecto.model.SpentTime;
import com.softtek.academy.proyecto.model.User;
import com.softtek.academy.proyecto.service.UserService;


@RestController
@RequestMapping("api/v1")
public class UserController {
	@Autowired
	UserService userService;
	
	@RequestMapping(value="")
	public String init() {
		userService.persistAll();
		return "BD Iniciada";
	}
	
	@RequestMapping(value = "users/{IS}/{mes}", method = RequestMethod.GET)
	public SpentTime UserByMonth(@PathVariable String IS,@PathVariable Integer mes) {
		try {
			Month m = Month.of(mes);
			User user = userService.getUserDB(IS);
			if(user!=null) {
				System.out.println("Usuario en la base de datos" + user);
				for(MonthTime mt : user.getMonths()) {
					if(mt.getMonth().equals(mes)) {
						SpentTime st = new SpentTime(m.toString(), mt.getTime(), IS);
						System.out.println("Sacado de la base de datos " + st);
						return st;
					}
				}
				Double time = userService.getHrsByMonth(user, mes.intValue());
				return new SpentTime(m.toString(), time, IS);
			}
			return new SpentTime("Usuario no encontrado", new Double(0), "Nadie");
		}catch(DateTimeException e) {
			return new SpentTime("No Existe el mes", new Double(0), IS);
		}
	}
	
	@RequestMapping(value = "users", method = RequestMethod.POST)
	public PostPeriodUserResponse LimitedByDatesTime(@RequestBody String postparams) {
		PostHolder ph = userService.createPostHolder(postparams);
		return userService.getPeriodTimeForUser(ph);
	}
	
	@RequestMapping(value="users/{mes}", method = RequestMethod.GET)
	public List<SpentTime> GetMonthTimeUsers(@PathVariable Integer mes){
		List<SpentTime> times = userService.getMonthTimeUsersDB(mes);
		return times;
	}
	@RequestMapping(value = "period", method = RequestMethod.POST)
	public List<PostPeriodUserResponse> PeriodUsers(@RequestBody String postparams){
		PostHolder ph = userService.createPostHolder(postparams);
		return userService.getPeriodTimeUsers(ph);
	}
	
}
