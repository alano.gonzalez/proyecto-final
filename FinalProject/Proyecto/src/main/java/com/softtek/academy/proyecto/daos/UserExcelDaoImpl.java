package com.softtek.academy.proyecto.daos;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

import com.softtek.academy.proyecto.model.User;

@Component("userExcelDao")
public class UserExcelDaoImpl implements UserExcelDao {
private List<User> users = new ArrayList<>();

	public UserExcelDaoImpl() {
		System.out.println("Iniciando userExcelDao");
		if(users.isEmpty()) {
			users = getAllUsers();
		}
	}
	
	@Override
	public User getUserExcel(String IS) {
		User user = new User();
		for(User u:users) {
			if(u.getIS().equals(IS)) {
				user = u;
				break;
			}
		}
		return user;
	}
	
	private static List<User> getAllUsers(){
		System.out.println("Leyendo archivo excel");
		List<User> users = new ArrayList();
		try {
			FileInputStream file = new FileInputStream(new File("RegistrosMomentum.xls"));
			
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;
			Map<Integer, User> mapa = new HashMap<>();
			User rep = new User();
			List<Date> registros = new ArrayList<>();
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				if(row.getRowNum()==0) {
					continue;
				}
				User u = new User();
				u.setId(new Integer(row.getCell(3).getStringCellValue()));
				u.setIS(row.getCell(2).getStringCellValue());
				String re =row.getCell(4).getStringCellValue();
				Date nr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(re);
				u.addRegister(nr);
				rep = mapa.put(u.getId(), u);
				if(rep==null) {
					registros = u.getRegisters();
				}
				if(rep!=null) {
					Date r = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(row.getCell(4).getStringCellValue());
					registros.add(r);
					rep.setRegisters(registros);
					mapa.replace(rep.getId(), rep);
				}
				
			}
			for(Entry<Integer, User> entry : mapa.entrySet()) {
			    User value = entry.getValue();
			    users.add(value);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public List<User> getUsers() {
		return getAllUsers();
	}
	
	@Override
	public List<User> getUsersNotReload(){
		return users;
	}
	
}
