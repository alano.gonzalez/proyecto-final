<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%
    	String contextPath = request.getContextPath();
    %>
<!DOCTYPE html>
<html>
<head>
	<style>
			form{
				margin: 200px;
			}
			button{
				margin: 0 auto;
				display: inline;
			}
			
			a{
			margin-right:100px;
			font-size:200%;
		}
		</style>
		
	<meta charset="ISO-8859-1">
	<title>Proyecto Momentum</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/16a3c2a219.js" crossorigin="anonymous"></script>
</head>
<body>
	<h1>Endpoint 1</h1>
	 	<a href="<%=contextPath%>"><i class="fas fa-home"></i></a>
	<form:form method="POST" action="userMonthview">
		<table>
			<tr>
				<td><form:label path="IS">IS</form:label></td>
				<td><form:input path="IS"/></td>
			</tr>
			<tr>
				<td><form:label path="mes">Mes</form:label></td>
				<td><form:input path="mes" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<input class="btn btn-primary" type="submit" value="Submit" />
				</td>
			</tr>
		</table>
	</form:form>

</body>
</html>