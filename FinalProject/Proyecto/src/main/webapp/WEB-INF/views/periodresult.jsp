<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%
    	String contextPath = request.getContextPath();
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Result</title>
	<style type="text/css">
		
		table{
			margin-top: 40px;
			font-size:120%;
			text-align:center;
		}
		a{
			margin-right:100px;
			font-size:200%;
		}
		
	</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/16a3c2a219.js" crossorigin="anonymous"></script>
</head>
<body>
 	<h1>Results</h1>
 	<a href="<%=contextPath%>"><i class="fas fa-home"></i></a>
	<table class="table table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">IS</th>
      <th scope="col">Registers</th>
      <th scope="col">Time</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${list}" var="user">   
    <tr>
      <th><c:out value="${user.getIS()}"/></th>
     <td> 
	     <table>
		      <c:forEach items="${user.getRegisters()}" var="reg">
		      		<tr><c:out value="${reg}"/><tr> | 
		      </c:forEach>
	      </table>
      </td>
      <th><c:out value="${user.getTime()}"/></th>
    </tr>
 </c:forEach>
  </tbody>
</table>
</body>
</html>