package com.softtek.academy.proyecto.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.proyecto.configuration.ProyectoConfiguration;
import com.softtek.academy.proyecto.daos.UserExcelDao;
import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.PostHolder;
import com.softtek.academy.proyecto.model.PostPeriodUserResponse;
import com.softtek.academy.proyecto.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProyectoConfiguration.class })
@WebAppConfiguration
public class UserServiceTest {
	
	
	@Autowired
	UserService userService;
	
	  @Test
	  public void TestFindByISExcel() {
	  
		  User uexpected = new User(); uexpected.setId(129); uexpected.setIS("EGGL");
		  
		  
		  User uactual = userService.getUserExcel("EGGL");
		  
		  
		  assertNotNull(uactual); assertEquals(uexpected.getIS(), uactual.getIS());
		  assertEquals(uexpected.getId(), uactual.getId()); 
	  }
	  
	  
	  @Test
	  public void TestCountByMonth() { 
		  Double expectedhrs = new Double(43.51277777777778); 
		  User uactual = userService.getUserExcel("ALAR1");
		  Double actualhrs = userService.getHrsByMonth(uactual, 4);
	  
		  assertNotNull(actualhrs); assertNotNull(uactual); assertEquals(expectedhrs,actualhrs); 
	}
	  
	  @Test
	  public void TestFindByISDB() {
	  
		  User uexpected = new User(); uexpected.setId(129); uexpected.setIS("EGGL");
		  
		  User actualexcel = userService.getUserExcel("EGGL"); Double actualhrs =
		  userService.getHrsByMonth(actualexcel, 4);
		  
		  uexpected.setMonths(actualexcel.getMonths());
		  
		  
		  userService.persistUserDB(actualexcel);
		  
		  User uactual = userService.getUserDB("EGGL");
		  
		  assertNotNull(uactual); assertEquals(uexpected.getId(), uactual.getId());
		  assertEquals(uexpected.getIS(), uactual.getIS());
		  assertEquals(uexpected.getMonths().size(), uactual.getMonths().size()); 
	}
	  
	  
	  @Test
	 public void TestPeriodTimeUser() { Double expected = new
		  Double(38.75138888888889); String postparams =
		  "IS=CXMG&startDate=2019-01-02&endDate=2019-01-07"; PostHolder ph =
		  userService.createPostHolder(postparams); Double actual =
		  userService.getPeriodTimeForUser(ph).getTime();
		  
		  assertNotNull(actual); assertEquals(expected, actual); 
	  }
	
}
