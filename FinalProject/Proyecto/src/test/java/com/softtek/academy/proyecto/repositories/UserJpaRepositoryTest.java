package com.softtek.academy.proyecto.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.proyecto.configuration.ProyectoConfiguration;
import com.softtek.academy.proyecto.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProyectoConfiguration.class })
@WebAppConfiguration
public class UserJpaRepositoryTest {

	@Autowired
	UserJpaRepository userJpaRepository;
	
	@Test
	public void TestGetUserByIS() {
		String IS = "CXMG";
		User expected = new User();
		expected.setId(101);
		expected.setIS(IS);
		
		User actual = userJpaRepository.findFirstByIS(IS);
		
		assertNotNull(actual);
		assertEquals(actual.getIS(), expected.getIS());
		assertEquals(actual.getId(), expected.getId());
	}
	
	@Test
	public void TestGetAllUsers() {
		List<User> users = userJpaRepository.findAll();
		
		assertNotNull(users);
		assertEquals(users.size(), 67);
	}
}
