package com.softtek.academy.proyecto.daos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.proyecto.configuration.ProyectoConfiguration;
import com.softtek.academy.proyecto.model.User;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProyectoConfiguration.class })
@WebAppConfiguration
public class UserExcelDaoTest {

	@Autowired
	UserExcelDao userExcelDao;
	
	@Test
	public void TestGetAllUsersExcel(){
		List<User> users = userExcelDao.getUsers();
		User user = userExcelDao.getUserExcel("ALAR1");
		boolean contains = false;
		
		for(User u : users) {
			if(u.getIS().equals(user.getIS())) {
				if(u.getRegisters().size() == user.getRegisters().size()) {
					contains = true;
				}
			}
		}
		
		assertNotNull(users);
		assertEquals(users.size(), 67);
		assertTrue(contains);
		
	}
}
