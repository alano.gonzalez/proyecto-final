package com.softtek.academy.proyecto.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.proyecto.configuration.ProyectoConfiguration;
import com.softtek.academy.proyecto.model.MonthTime;
import com.softtek.academy.proyecto.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProyectoConfiguration.class })
@WebAppConfiguration
public class MonthTimeJpaRepositoryTest {

	@Autowired
	MonthTimeJpaRepository monthTimeJpaRepository;

	@Test
	public void TestGetFindallByUser() {
		
		User user = new User();
		user.setId(101);
		user.setIS("CXMG");
		List<MonthTime> months = monthTimeJpaRepository.findAllByUser(user);
		
		assertNotNull(months);
		assertEquals(months.size(), 4);
	}
	
}
