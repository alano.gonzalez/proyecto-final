create table User(
	Id int,
    uIS varchar(10),
    constraint PK_USER primary key(Id)
);



create table MonthTime(
	MTId int auto_increment,
    monthNum int,
	timespent double,
    userId int,
    constraint PK_MT primary key(MTId),
    constraint FK_MT_U foreign key(userId) references User(Id)
);

select * from User;

select count(*) from User;

select * from MonthTime;



drop table User;
drop table MonthTime;

truncate table User;
truncate table MonthTime;
